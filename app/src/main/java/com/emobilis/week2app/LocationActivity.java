package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LocationActivity extends AppCompatActivity {
    TextView textLocation;
    //variable to store decoded location
    String address;
    //request code to match the permission
    private static final int REQUEST_CODE_LOCATION_PERMISSION = 1;
    //create a reference to the Geocoder class
    Geocoder geocoder;
    //List reference to store the info that we get about the location
    List<Address> addressList;
    //variables to hold users latitude and longitude
    double lat,lon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        //ref to textview
        textLocation = findViewById(R.id.location);
        //create an instance of the geocoder
        geocoder = new Geocoder(this, Locale.getDefault());

        //call the method to request permissions from the user
        turnOnLocation();
    }

    //request permissions from the user
    private void turnOnLocation() {
        //use location manager to get Location Service
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        //first i check if i can get the location from one the gps or the network being used
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            //launch a dialog with two options yes or no
            //alert dialog builder
            //new instance of The AlertDialog class
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //customize the dialog
            builder.setTitle("Location Permission");
            builder.setMessage("Location needs to be enabled inorder for app to be used efficiently");
            //set up the options
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //take user to settings app to turn on location
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(LocationActivity.this, "Location needs to be on for app to function well", Toast.LENGTH_SHORT).show();
                }
            });

            builder.setNeutralButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    LocationActivity.super.onBackPressed();
                }
            });

            //execute the builder
            Dialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();


        } else {


            Toast.makeText(this, "Location is actually on.", Toast.LENGTH_SHORT).show();


        }


    }

    public void getCurrent(View v){
        //call the method to ask for users permission
        getLocations();
    }
    //user permission to get the fine location and maybe coarse
    private void getLocations() {
        //alerDialog
        new AlertDialog.Builder(this)
                .setTitle("Allow app to get your detailed location")
                .setMessage("will allow app to work better")

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //i check whether permission is granted

                        if (ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                            //from this point it will move to the onRequestPermissionResult method
                            ActivityCompat.requestPermissions(
                                    LocationActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION
                            );

                        } else {
                            //if the permission is already set
                            getCurrentLocation();
                        }

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .setIcon(R.drawable.ic_directions_bike_black_24dp)
                .setCancelable(false)
                .show();
    }


    //method to handle users permission choice


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION && grantResults.length > 0){
               getCurrentLocation();
        } else {
            Toast.makeText(this, "User denied permission", Toast.LENGTH_SHORT).show();
        }
    }

    //this fetches longitude and lat and use geocoder to get  finer info
    private void getCurrentLocation() {
        //request for user location from Android OS
        final LocationRequest locationRequest = new LocationRequest(); //create instance of LocationRequest class
        //set the metadata
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setInterval(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(LocationActivity.this).requestLocationUpdates(locationRequest, new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                //we can get lat lon , geocoder to convert this to an actual address
                LocationServices.getFusedLocationProviderClient(LocationActivity.this).removeLocationUpdates(this);
                //checking if location result actually has a result
                if (locationResult != null && locationResult.getLocations().size() > 0){
                    //set up location index which will allow us to get the location lat and longitude
                    int latestlocationIndex = locationResult.getLocations().size() - 1; //removing the last known location from our array list

                    //get user latitude and longitude
                    lat = locationResult.getLocations().get(latestlocationIndex).getLatitude();
                    Log.d("LocationActivity","lat is " + lat);
                    lon = locationResult.getLocations().get(latestlocationIndex).getLongitude();
                    Log.d("LocationActivity","lat is " + lon);

                    //use geocoder class to make sense out of the lat and lon
                    try{
                        //reverse geocoding
                        addressList = geocoder.getFromLocation(lat,lon,1); //i expect one result from geocoder execution
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    //using the list i can access the details of the user current location
                    //address
                    address = addressList.get(0).getAddressLine(0);

                    //get other info about users current location
                    String city = addressList.get(0).getLocality();
                    String state = addressList.get(0).getAdminArea();
                    String country = addressList.get(0).getCountryName();
                    String postalCode = addressList.get(0).getPostalCode();
                    String knownName = addressList.get(0).getFeatureName();

                    textLocation.setText(address);


                }
            }
        }, Looper.getMainLooper());
    }
}
