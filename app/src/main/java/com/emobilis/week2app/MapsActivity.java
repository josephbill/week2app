package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
        {

    private GoogleMap mMap;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //checking if device can support maps if so asking permission from the user to get their current location
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        turnOnLocation();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


            //request permissions from the user
            private void turnOnLocation() {
                //use location manager to get Location Service
                LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
                //first i check if i can get the location from one the gps or the network being used
                if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                    //launch a dialog with two options yes or no
                    //alert dialog builder
                    //new instance of The AlertDialog class
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    //customize the dialog
                    builder.setTitle("Location Permission");
                    builder.setMessage("Location needs to be enabled inorder for app to be used efficiently");
                    //set up the options
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //take user to settings app to turn on location
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    });

                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MapsActivity.this, "Location needs to be on for app to function well", Toast.LENGTH_SHORT).show();
                        }
                    });

                    builder.setNeutralButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MapsActivity.super.onBackPressed();
                        }
                    });

                    //execute the builder
                    Dialog alertDialog = builder.create();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();


                } else {


                    Toast.makeText(this, "Location is actually on.", Toast.LENGTH_SHORT).show();


                }


            }

            public boolean checkLocationPermission() {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    //launch pop up to ask user for permission
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_LOCATION);
                    } else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_PERMISSIONS_REQUEST_LOCATION);
                    }

                    return false;

                } else {
                    return true;
                }

            }

            @Override
            public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                switch (requestCode) {
                    case MY_PERMISSIONS_REQUEST_LOCATION: {
                        if (grantResults.length > 0
                                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            if (ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                                    == PackageManager.PERMISSION_GRANTED) {
                                if (mGoogleApiClient == null) {
                                    buildGoogleApiClient();
                                }
                                mMap.setMyLocationEnabled(true);
                            }
                        } else {
                            Toast.makeText(this, "permission denied, app might not work well",
                                    Toast.LENGTH_LONG).show();
                        }
                        return;
                    }
                }
            }

            @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //setting up our map type
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //giving metadata to our map
        mMap.getUiSettings().setZoomControlsEnabled(true); //zoom controls
        mMap.getUiSettings().setAllGesturesEnabled(true); //gestures
        mMap.getUiSettings().setCompassEnabled(true);  //compass

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    //buildGoogleApiClient
            //request to the google maps server API
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

            @Override
            public void onConnected(@Nullable Bundle bundle) {
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(1000);
                mLocationRequest.setFastestInterval(1000);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                //if granted permission get the user location and also any change in this location
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                            mLocationRequest, this);
                }
            }

            @Override
            public void onConnectionSuspended(int i) {

            }

            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            }

            @Override
            public void onLocationChanged(Location location) {
                  //we check state of the marker
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //showing users current location using marker
                //getting the users lat and lng using a new instance of the LATLNG modle
                LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
                //setting position of marker
                MarkerOptions markerOptions = new MarkerOptions();
                //set marker to lat and lng of user
                markerOptions.position(latLng);
                //setting an instance of the location manager class to get the location services from our android device
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                //saving location service provider in a variable tag
                String provider = locationManager.getBestProvider(new Criteria(), true);
                //checking permission again
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                //instance of the Location CLass to get the last known location
                Location locations = locationManager.getLastKnownLocation(provider);
                //storing our location results inside a list
                List<String> providerList = locationManager.getAllProviders();
                //we want to get user lat and lng which we can convert or decode using a GeoCoder instance
                if (null != locations && null != providerList && providerList.size() > 0){
                     //get lat and lng
                    double latitude = locations.getLatitude();
                    double longitude = location.getLongitude();
                    //new insatance of the GeoCoder
                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                    //decoding lat and lng
                    try{
                        List<Address> addressList = geocoder.getFromLocation(latitude,longitude,1);
                        if (null != addressList && addressList.size() > 0){
                            String address = addressList.get(0).getAddressLine(0);
                            //get other info about users current location
                            String city = addressList.get(0).getLocality();
                            String state = addressList.get(0).getAdminArea();
                            String country = addressList.get(0).getCountryName();
                            String postalCode = addressList.get(0).getPostalCode();
                            String knownName = addressList.get(0).getFeatureName();
                            //set this info to our marker
                            markerOptions.title("Latitude and Longitude: " + latLng + "city: " + city + "Complete Address: " + address);
                            //giving the marker an icon
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                            //giving the map the marker and also setting some animations]
                            mCurrLocationMarker = mMap.addMarker(markerOptions);
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                            //removing extra locations when the user has moved
                            if (mGoogleApiClient != null) {
                                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                                        this);
                            }
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

            }
        }
