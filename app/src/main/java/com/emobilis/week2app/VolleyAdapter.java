package com.emobilis.week2app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class VolleyAdapter extends RecyclerView.Adapter<VolleyAdapter.VolleyViewHolder> {
    //constructor variable
     Context context;
     private ArrayList<VolleyModel> volleyModels;

     //constructor
    public VolleyAdapter(Context context,ArrayList<VolleyModel> volleyModelArrayList){
        this.context = context;
        this.volleyModels = volleyModelArrayList;
    }


    //view holder
    public class VolleyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tvCreator,tvTags,tvLikes;

        public VolleyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            tvCreator = itemView.findViewById(R.id.text_view_creator);
            tvTags = itemView.findViewById(R.id.text_view_tags);
            tvLikes = itemView.findViewById(R.id.text_view_likes);
        }
    }


    @NonNull
    @Override
    public VolleyAdapter.VolleyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.volley_item, parent, false);
        return new VolleyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VolleyAdapter.VolleyViewHolder holder, int position) {
        //set position of items
        VolleyModel volleyModel = volleyModels.get(position);
        //variables to store the data response from my model class
        String imageUrl = volleyModel.getImageURL();
        String creator = volleyModel.getCreator();
        String tags = volleyModel.getTags();
        int likes = volleyModel.getLikes();

        //set data to my views
        holder.tvLikes.setText("Likes: " + likes);
        holder.tvTags.setText("Tags: " + tags);
        holder.tvCreator.setText("Posted By: " + creator);

        //glide
        Glide
                .with(context)
                .load(imageUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_directions_bike_black_24dp)
                .into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return volleyModels.size();
    }
}
