package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FastNetworkingGetActivity extends AppCompatActivity {
    TextView textView,textViewArray;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_networking);

        textView = findViewById(R.id.textGet);
        textViewArray = findViewById(R.id.textArray);
        //intialize android networking
        AndroidNetworking.initialize(getApplicationContext());

        //method GET
        getData();

        //method GET using array
        getDataArray();

    }

    private void getDataArray() {
        //define the API URL
        String URL = "https://reqres.in/api/users?page=2";
        //android fast
        AndroidNetworking.get(URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("data");
                            //for loop to loop array
                            for (int i= 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String first_name = jsonObject.getString("first_name");
                                String last_name = jsonObject.getString("last_name");
                                //set the data to my textview
                                textViewArray.append("First Name " + first_name + "Last names " + last_name);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(FastNetworkingGetActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                        Log.d("get array", "what is the error "  + anError.getErrorBody() );
                    }
                });

    }

    private void getData() {
        //define the API URL
        String URL = "https://reqres.in/api/users?page=2";
        //android fast
        AndroidNetworking.get(URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //dealing with JSON OBJECT
                        try {
                            String total_pages = response.getString("total_pages");
                            String per_page = response.getString("per_page");
                            //set this text view container
                            textView.append("Total pages " + total_pages + "Per page " + per_page);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(FastNetworkingGetActivity.this, "Error occured", Toast.LENGTH_SHORT).show();
                        Log.d("get object", "what is the error "  + anError.getErrorBody() );
                    }
                });
    }
}
