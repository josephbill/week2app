package com.emobilis.week2app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteDatabaseHelper extends SQLiteOpenHelper {
    //variables to store my database name and my version
    public static final String DATABASE_NAME = "grocerylist.db";
    public static final int DATABASE_VERSION = 1;

    public SqliteDatabaseHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    //creating a database
    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_GROCERYLIST_TABLE = "CREATE TABLE " +
                SqliteContract.SqLiteEntry.TABLE_NAME + " (" +
                SqliteContract.SqLiteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SqliteContract.SqLiteEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                SqliteContract.SqLiteEntry.COLUMN_AMOUNT + " INTEGER NOT NULL, " +
                SqliteContract.SqLiteEntry.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" +
                ");";
        db.execSQL(SQL_CREATE_GROCERYLIST_TABLE);
    }


    //if u want to delete the database this is the method to execute
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SqliteContract.SqLiteEntry.TABLE_NAME);
        onCreate(db);
    }
}
