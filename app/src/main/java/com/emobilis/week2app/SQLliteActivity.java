package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SQLliteActivity extends AppCompatActivity {
    //declare our views / implementations
    private SQLiteDatabase mDatabase;
    private SqliteAdapter mAdapter;
    private EditText mEditTextName;
    private TextView mTextViewAmount;
    private int mAmount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_s_q_llite);
        //create a new instance of the Database helper class
        SqliteDatabaseHelper dbHelper = new SqliteDatabaseHelper(this);
        //attach our database to our databaseHelper class
        mDatabase = dbHelper.getWritableDatabase();
        //find the view of the recyclerview
        RecyclerView recyclerView = findViewById(R.id.recyclerSql);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //create a new instance of the adapter class
        mAdapter = new SqliteAdapter(this, getAllItems());
        //set adapter to the recyclerview
        recyclerView.setAdapter(mAdapter);

        //swipe to delete
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //remove item based on id
                removeItem((long) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(recyclerView);

        //find extra views by id
        mEditTextName = findViewById(R.id.edittext_name);
        mTextViewAmount = findViewById(R.id.textview_amount);

        Button buttonIncrease = findViewById(R.id.button_increase);
        Button buttonDecrease = findViewById(R.id.button_decrease);
        Button buttonAdd = findViewById(R.id.button_add);

        buttonIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increase();
            }
        });
        buttonDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrease();
            }
        });
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });
    }

    private void increase(){
        mAmount++;
        mTextViewAmount.setText(String.valueOf(mAmount));
    }

    private void decrease(){
        if (mAmount > 0) {
            mAmount--;
            mTextViewAmount.setText(String.valueOf(mAmount));
        }
    }

    private void removeItem(long id) {
        mDatabase.delete(SqliteContract.SqLiteEntry.TABLE_NAME,
                SqliteContract.SqLiteEntry._ID + "=" + id, null);
        mAdapter.swapCursor(getAllItems());
    }


    private void addItem(){
        if (mEditTextName.getText().toString().trim().length() == 0 || mAmount == 0) {
            return;
        }

        // get user input
        String name = mEditTextName.getText().toString().trim();
        //creating a new instance of the contentValues class
        ContentValues cv = new ContentValues();
        //now using content Values class i can hold data and insert it dynamically to my sqllite
        cv.put(SqliteContract.SqLiteEntry.COLUMN_NAME, name);
        cv.put(SqliteContract.SqLiteEntry.COLUMN_AMOUNT, mAmount);
        //insert data to sqlite db
        mDatabase.insert(SqliteContract.SqLiteEntry.TABLE_NAME, null, cv);
        //notify our recyclerview that a new data has been added
        mAdapter.swapCursor(getAllItems());
        //clear the edit text if a record is entered successfully
        mEditTextName.getText().clear();
    }

    //query the db to fetch our records and then records can be used and set to my recylerview
    private Cursor getAllItems(){
        return mDatabase.query(
                SqliteContract.SqLiteEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                SqliteContract.SqLiteEntry.COLUMN_TIMESTAMP + " DESC"
        );    }
}
