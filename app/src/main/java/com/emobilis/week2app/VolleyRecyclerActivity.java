package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class VolleyRecyclerActivity extends AppCompatActivity {
     //declare tools
     private RecyclerView mRecyclerView;
    private VolleyAdapter volleyAdapter;
    private ArrayList<VolleyModel> volleyModelArrayList;
    private RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley_recycler);

        mRecyclerView = findViewById(R.id.recyclerVolley);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        volleyModelArrayList = new ArrayList<>();

        //create an instance of the volley request queu
        mRequestQueue = Volley.newRequestQueue(this);
        //run a method that will handle our get request
        parseJSON();
    }
     //get request
    private void parseJSON() {
        //declare the API URL
        String url = "https://pixabay.com/api/?key=5303976-fd6581ad4ac165d1b75cc15b3&q=kitten&image_type=photo&pretty=true";

        //use volley  //creating a new instance of the JSONObjectRequest class from volley
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                       try {
                           //create a ref to my array in the JSON structure
                           JSONArray jsonArrayHits = response.getJSONArray("hits");
                           //for loop to iterate above array
                           for (int i=0; i < jsonArrayHits.length(); i++){
                               //creating a reference for the object
                               JSONObject jsonObject = jsonArrayHits.getJSONObject(i);
                               //save the info of the object data inside variables
                               int id = jsonObject.getInt("id");
                               String creator = jsonObject.getString("user");
                               String tags = jsonObject.getString("tags");
                               String imageUrl = jsonObject.getString("webformatURL");
                               int likes = jsonObject.getInt("likes");

                               //add content to the model class
                               volleyModelArrayList.add(new VolleyModel(imageUrl,tags,likes,creator));

                               //create an instance of the adapter
                               volleyAdapter = new VolleyAdapter(VolleyRecyclerActivity.this,volleyModelArrayList);
                               //set the adapter to recyclerview
                               mRecyclerView.setAdapter(volleyAdapter);

                           }

                       }catch (Exception e){
                           e.printStackTrace();
                       }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VolleyRecyclerActivity.this, "Error is " + error, Toast.LENGTH_SHORT).show();
            }
        });
        //adding request to our volley qeue  //executing the volley instance
        mRequestQueue.add(jsonObjectRequest);
    }
}
