package com.emobilis.week2app;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ShareViewModel extends ViewModel {
    //create a new instance of the MutableLiveData subclass
    private MutableLiveData<CharSequence> text = new MutableLiveData<>();

    //set text

    public void setText(CharSequence input){
        text.setValue(input);
    }


    //get text
    public LiveData<CharSequence> getText(){
        return text;
    }
}
