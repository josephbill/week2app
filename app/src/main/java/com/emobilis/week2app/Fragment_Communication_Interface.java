package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Fragment_Communication_Interface extends AppCompatActivity implements FragmentCommunication_A.FragmentAListener, FragmentCommunication_B.FragmentBListener {
    //create references to the fragments
    FragmentCommunication_A fragmentCommunication_a;
    FragmentCommunication_B fragmentCommunication_b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment__communication__interface);

        //create instances of the Fragment classes
        fragmentCommunication_a = new FragmentCommunication_A();
        fragmentCommunication_b = new FragmentCommunication_B();

        //we load our fragments to the activity
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_a, fragmentCommunication_a)
                .replace(R.id.container_b, fragmentCommunication_b)
                .commit();
    }

    @Override
    public void onInputA(CharSequence input) {
         fragmentCommunication_b.updateEditText(input);
    }

    @Override
    public void onInputB(CharSequence input) {
         fragmentCommunication_a.updateEditText(input);
    }
}
