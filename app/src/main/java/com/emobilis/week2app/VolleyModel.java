package com.emobilis.week2app;

public class VolleyModel {
    //variables to store my info
    private String imageURL;
    private String tags;
    private int likes;
    private String creator;

    //constructor
    public VolleyModel(String imageURL,String tags,int likes,String creator){
        this.imageURL = imageURL;
        this.tags = tags;
        this.likes = likes;
        this.creator = creator;
    }

    //get and sets

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
