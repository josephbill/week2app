package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation_view);

        //find the ref to the bottom nav
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        //begin transactions of the fragments
        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.bottom_frame,
                    new FragmentA()).commit();
        }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    //switch statements
                    switch (item.getItemId()){
                        case R.id.nav_fragmentA:
                            Intent intents = new Intent(BottomNavigationActivity.this,TabLayoutActivity.class);
                            startActivity(intents);
                            break;
                        case R.id.nav_fragmentB:
                            Intent intentShare = new Intent(BottomNavigationActivity.this,SharedPreferencesActivity.class);
                            startActivity(intentShare);
                            break;
                        case R.id.nav_fragmentC:
                            Intent intentSql = new Intent(BottomNavigationActivity.this,SQLliteActivity.class);
                            startActivity(intentSql);
                            break;
                        case R.id.nav_web:
                            Intent intent = new Intent(BottomNavigationActivity.this,WebActivity.class);
                            startActivity(intent);
                            break;

                        default:
                            Toast.makeText(BottomNavigationActivity.this, "Default clicked", Toast.LENGTH_SHORT).show();

                    }
                    return false;
                }
            };

}
