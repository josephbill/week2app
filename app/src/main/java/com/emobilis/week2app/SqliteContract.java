package com.emobilis.week2app;

import android.provider.BaseColumns;

public class SqliteContract {
    //empty constructor
    private SqliteContract(){

    }
    //create a subclass to implement BaseColumn
    public static final class SqLiteEntry implements BaseColumns{
        public static final String TABLE_NAME = "groceryList";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_AMOUNT = "amount";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }
}