package com.emobilis.week2app;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentCommunication_B extends Fragment {
    //declare the views
    EditText name;
    Button btnSubmit;
    //ref to the interface
    FragmentBListener fragmentBListener;

    //interface for this class
    public interface FragmentBListener{
        void onInputB(CharSequence input);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_communication__b, container, false);
        //ref to my views
        name = v.findViewById(R.id.edit_text);
        btnSubmit = v.findViewById(R.id.button_ok);
        //on click
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get the user input
                CharSequence input = name.getText().toString().trim();
                //pass the input to the interface method
                fragmentBListener.onInputB(input);
            }
        });

        return v;
    }


    //updating the new text to the edit text
    public void updateEditText(CharSequence newText){
        name.setText(newText);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //create a ref to the instance of the interface method in this class
        if (context instanceof FragmentBListener) {
            //create a new instance of the interface method
            fragmentBListener = (FragmentBListener) context;
        } else {
            //error catching
            throw new RuntimeException(context.toString() + " Must implement Fragment listner");
        }

    }

    //detach

    @Override
    public void onDetach() {
        super.onDetach();
        //when app is closed close instance of the interface
        fragmentBListener = null;
    }
}
