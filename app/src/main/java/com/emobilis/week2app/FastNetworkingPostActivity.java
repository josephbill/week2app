package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

public class FastNetworkingPostActivity extends AppCompatActivity {
    //declare the views
    EditText etName,etJob;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_networking_post);

        etName = findViewById(R.id.name);
        etJob = findViewById(R.id.job);
        btnSubmit = findViewById(R.id.btnSubmit);

        AndroidNetworking.initialize(getApplicationContext());

        //set the onclick listener for button
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //method to post
                postData();
            }
        });
    }

    private void postData() {
        //get user input
        String name_of_user = etName.getText().toString().trim();
        String job_user = etJob.getText().toString().trim();
        //reference to API
        String URL = "https://reqres.in/api/users";

        //android networking post
        AndroidNetworking.post(URL)
                .addBodyParameter("name",name_of_user)
                .addBodyParameter("job",job_user)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(FastNetworkingPostActivity.this, "Data has been posted " + response.toString(), Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(FastNetworkingPostActivity.this, "Error occurred data not submitted " + anError.getErrorBody(), Toast.LENGTH_LONG).show();

                    }
                });

        //to upload image
//        AndroidNetworking.upload(URL)
//                .addMultipartParameter("name", name_of_user)
//                .addMultipartParameter("job", job_user)
//                .addMultipartFile("file",file0)
    }
}
