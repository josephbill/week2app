package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class Fragments_Communication_View_Model extends AppCompatActivity {
    //declare the ref to fragments
    Fragment_Communication_Model_A fragment_communication_model_a;
    Fragment_Communication_Model_B fragment_communication_model_b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragments__communication__view__model);
        //need to create instances of the fragment classes
        fragment_communication_model_a = new Fragment_Communication_Model_A();
        fragment_communication_model_b = new Fragment_Communication_Model_B();

        //load the fragments to the activity view
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_a, fragment_communication_model_a )
                .replace(R.id.container_b, fragment_communication_model_b )
                .commit();
    }
}
