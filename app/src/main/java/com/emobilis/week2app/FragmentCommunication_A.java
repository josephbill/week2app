package com.emobilis.week2app;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FragmentCommunication_A extends Fragment {
    private EditText name;
    private Button btnSubmit;
    //declare the ref to the interface method
    FragmentAListener fragmentAListener;

    //i create an interface to communicate between the fragments
    public interface FragmentAListener{
        void onInputA(CharSequence input);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v =  inflater.inflate(R.layout.fragment_communication__a, container, false);
       //finding views in fragment A
        name = v.findViewById(R.id.edit_text);
        btnSubmit = v.findViewById(R.id.button_ok);
        //set on click listener for the button
        //where we will be getting the input from the user
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getting users input
                CharSequence input = name.getText().toString().trim();
                //call my interface method
                fragmentAListener.onInputA(input);
            }
        });

        //return view
        return v;
    }

    //updating the new text to the edit text
    public void updateEditText(CharSequence newText){
        name.setText(newText);
    }

    //attach

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //create a ref to the instance of the interface method in this class
        if (context instanceof FragmentAListener) {
            //create a new instance of the interface method
            fragmentAListener = (FragmentAListener) context;
        } else {
            //error catching
            throw new RuntimeException(context.toString() + " Must implement Fragment listner");
        }

    }

    //detach

    @Override
    public void onDetach() {
        super.onDetach();
        //when app is closed close instance of the interface
        fragmentAListener = null;
    }
}
