package com.emobilis.week2app;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
    //declare the instance of this file
    private static VolleySingleton volleySingelton;
    private RequestQueue requestQueue;

    //constructor to be used by other activity requesting network service
    private VolleySingleton(Context context){
        //create the new instance
        requestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized VolleySingleton getInstance(Context context){
        //we check if the activity requesting this file has volley instance attached to it
        if (volleySingelton == null){
            volleySingelton = new VolleySingleton(context);
        }

        return volleySingelton;
    }

    //create the instance of the request queue
    public RequestQueue getRequestQueue(){
        return requestQueue;
    }
}
