package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class SharedPreferencesActivity extends AppCompatActivity {
   //declare the views
   private TextView textView;
    private EditText editText;
    private Button applyTextButton;
    private Button saveButton;
    private Switch switch1;
    //declare a shared preference ref
    public static final String SHARED_PREFS = "sharedPrefs";
    //stored a placeholder text plus a placeholder switch state
    public static final String TEXT = "text";
    public static final String SWITCH1 = "switch1";
    //variables to store the users input on text, switch widget
    private String text;
    private boolean switchOnOff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);

        //find the views by id
        textView = (TextView) findViewById(R.id.textview);
        editText = (EditText) findViewById(R.id.edittext);
        applyTextButton = (Button) findViewById(R.id.apply_text_button);
        saveButton = (Button) findViewById(R.id.save_button);
        switch1 = (Switch) findViewById(R.id.switch1);

        //onclick listener for apply btn
        applyTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(editText.getText().toString().trim());
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
        //to see what user stores in my shared prefs
        loadData();
        //to reset the content when user enter new input
        updateViews();
    }
    //to save the content to shared pref
    private void saveData(){
        //new instance of the sp class
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //place data to my shared pref reference
        editor.putString(TEXT,textView.getText().toString().trim());
        editor.putBoolean(SWITCH1, switch1.isChecked());

        //apply the data to shared prefs
        editor.apply();


        //Toast
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();

    }

    private void loadData(){
        //new instance of the class
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT,"");
        switchOnOff = sharedPreferences.getBoolean(SWITCH1,false);
    }

    private void updateViews() {
        textView.setText(text);
        switch1.setChecked(switchOnOff);
    }

}
