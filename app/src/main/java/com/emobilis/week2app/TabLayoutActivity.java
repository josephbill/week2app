package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

public class TabLayoutActivity extends AppCompatActivity {

    //declare sections pager adapter
    private SectionsPagerAdapter sectionsPagerAdapter;
    //view pager
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);
        //find toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(sectionsPagerAdapter);
        //find the tablayout and set viewpager to it
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.bottom_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //declare a variable to store the menu ids
        int id = item.getItemId();
        //if to switch
        if (id == R.id.nav_fragmentA){
            Toast.makeText(this, "Fragment Clicked", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_web){
            Intent intent = new Intent(TabLayoutActivity.this,WebActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    //creating a fragment for a simple view
    public static class PlaceholderFragment extends Fragment{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        //empty constructor
        public PlaceholderFragment() {
        }

        //create a new instance of this fragment subclass to attach that fragment to the view in relation to the assigned section no
        public static PlaceholderFragment newInstance(int sectionNumber){
            //new instance of the placeholder fragment
            PlaceholderFragment fragment = new PlaceholderFragment();
            //new instance of the bundle
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER,sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @SuppressLint("StringFormatInvalid")
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            //inflate the layout
            View rootView = inflater.inflate(R.layout.fragment_a, container,false);
            TextView textView = rootView.findViewById(R.id.textSwitch);
            textView.setText(getString(R.string.app_name, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }


    }

    //set up sectionspageradapter
    public static class SectionsPagerAdapter extends FragmentPagerAdapter{

        //constructor
        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch(position){
                case 0:
                    fragment = new FragmentA();
                    break;
                case 1:
                    fragment =  new FragmentB();

                    break;
                case 2:
                    fragment = new FragmentC();
                    break;

                default:


            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "Fragment A";
                case 1:
                    return "Fragment B";
                case 2:
                    return "Fragment C";
            }
            return null;
        }
    }
}
