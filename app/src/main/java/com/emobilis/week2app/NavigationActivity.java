package com.emobilis.week2app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

public class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //declare the drawer layout
    private DrawerLayout drawerLayout;
    //auth
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        //find the ref to our toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        //find navigation view
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);


        //toggle action  //create a new instance of the ActionBarDrawerToggle class
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        //loading a default fragment
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FragmentA()).commit();
            navigationView.setCheckedItem(R.id.nav_fragment_a);
        }

        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();

    }

    //backpressed

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        //switch the views using the switch statement
        switch (item.getItemId()){
            case R.id.nav_fragment_a:
                Intent intents = new Intent(NavigationActivity.this,BottomNavigationActivity.class);
                startActivity(intents);
                break;
            case R.id.nav_fragment_b:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentB()).commit();
                break;
            case R.id.nav_fragment_c:
                Intent maps = new Intent(NavigationActivity.this,MapsActivity.class);
                startActivity(maps);
                break;
            case R.id.frag_interface:
                Intent intentFrag = new Intent(NavigationActivity.this,Fragment_Communication_Interface.class);
                startActivity(intentFrag);
                break;
            case R.id.web:
                //to go an activity
                Intent intent = new Intent(NavigationActivity.this,WebActivity.class);
                startActivity(intent);
                break;
            case R.id.frag_shareview:
                Intent intentShare = new Intent(NavigationActivity.this,Fragments_Communication_View_Model.class);
                startActivity(intentShare);
                break;
            case R.id.image_picker:
                Intent intentImage = new Intent(NavigationActivity.this,ImagePickerActivity.class);
                startActivity(intentImage);
                break;
            case R.id.location:
                Intent intentLocation = new Intent(NavigationActivity.this,LocationActivity.class);
                startActivity(intentLocation);
                break;
            case R.id.nav_volley:
                Intent intentVolley = new Intent(NavigationActivity.this,VolleyRecyclerActivity.class);
                startActivity(intentVolley);
                break;
            case R.id.nav_volley_post:
                Intent intentVolleyPost = new Intent(NavigationActivity.this,VolleyPostActivity.class);
                startActivity(intentVolleyPost);
                break;
            case R.id.nav_fast_get:
                Intent intentFastGET = new Intent(NavigationActivity.this,FastNetworkingGetActivity.class);
                startActivity(intentFastGET);
                break;
            case R.id.nav_fast_post:
                Intent intentFastPost = new Intent(NavigationActivity.this,FastNetworkingPostActivity.class);
                startActivity(intentFastPost);
                break;
            case R.id.logOut:
                auth.signOut();
                finishAffinity();
                break;

            default:
                Toast.makeText(this, "No link selected ", Toast.LENGTH_SHORT).show();
        }


        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }
}
