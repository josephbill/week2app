package com.emobilis.week2app;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SqliteAdapter extends RecyclerView.Adapter<SqliteAdapter.SqliteViewHolder> {
    private Context mContext;
    private Cursor mCursor;

    public SqliteAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
    }

    //view holder
    public class SqliteViewHolder extends RecyclerView.ViewHolder{
        //declare views in recycled item
        TextView amount,name;
        public SqliteViewHolder(@NonNull View itemView) {
            super(itemView);
            amount = itemView.findViewById(R.id.textview_amount_item);
            name = itemView.findViewById(R.id.textview_name_item);
        }
    }


    @NonNull
    @Override
    public SqliteAdapter.SqliteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.shopping_item, parent, false);
        return new SqliteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SqliteAdapter.SqliteViewHolder holder, int position) {
        if (!mCursor.moveToPosition(position)) {
            return;
        }

        //store user input in this variables
        String name = mCursor.getString(mCursor.getColumnIndex(SqliteContract.SqLiteEntry.COLUMN_NAME));
        int amount = mCursor.getInt(mCursor.getColumnIndex(SqliteContract.SqLiteEntry.COLUMN_AMOUNT));
        long id = mCursor.getLong(mCursor.getColumnIndex(SqliteContract.SqLiteEntry._ID));

        //set data to my views
        holder.amount.setText(String.valueOf(amount));
        holder.name.setText(name);
        holder.itemView.setTag(id);
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) {
            mCursor.close();
        }

        mCursor = newCursor;
        if (newCursor != null) {
            notifyDataSetChanged();
        }
    }
}
