package com.emobilis.week2app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public class VolleyPostActivity extends AppCompatActivity {
   //declare the views
    EditText etName,etJob;
    Button btnSubmit;
    //request queue
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley_post);

        etName = findViewById(R.id.name);
        etJob = findViewById(R.id.job);
        btnSubmit = findViewById(R.id.btnSubmit);

        //volleySingleton file instance , getInstance method
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        //set the onclick listener for button
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //method to post
                postData();
            }
        });

    }

    private void postData() {
        //get user input
        String name_of_user = etName.getText().toString().trim();
        String job_user = etJob.getText().toString().trim();
        //reference to API
        String URL = "https://reqres.in/api/users";
        //create a JSONObject
        JSONObject jsonObject = new JSONObject();
        //try and catch put the values to specified parameters
        try{
            //using the put to post data
            jsonObject.put("name",name_of_user);
            jsonObject.put("job",job_user);

        }catch (Exception e){
            e.printStackTrace();
        }

        //jsonObjectrequest listener
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(VolleyPostActivity.this, "Data has been posted " + response.toString(), Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(VolleyPostActivity.this, "Error occurred data not submitted " + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });
        //adding the request to ,my request queue
        requestQueue.add(jsonObjectRequest);
    }
}
